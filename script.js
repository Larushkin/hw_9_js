'use strict'

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// С помощью метода document.createElement("div");

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// разбирает указанный текст как HTML и вставляет полученные nodes в DOM дерево в указанную позицию.
// 	beforebegin - до открывающего тега,
// 	afterbegin - сразу после открывающего тега(перед первым потомком),
// 	beforeend - сразу перед закрывающим тегом (после последнего потомка),
// 	afterend - после закрывающего тега.


// 3. Як можна видалити елемент зі сторінки?
//  С помощью метода remove();


//                  Основное задание 

// const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


// let list = document.createElement("ul");

// document.body.append(list) 

// function createList(array, place) {
//  	let arrlist = [];
//  	array.forEach(el => {
//  		arrlist.push(`<li>${el}</li>`)
//  	})
//  	return place.innerHTML = arrlist.join("");
// }

// createList(arr, list);


//                      Дополнительное задание 


const arr = [ "Kiev", ["Borispol", "Irpin"], "Kharkiv", "Odessa", "Lviv", "Dnepr",];

let list = document.createElement("ul");
document.body.append(list);
list.style.fontSize='20px';

function createList(array) {

	let newArr = array.map(el => {

		if (!Array.isArray(el)) {
			return `<li>${el}</li>`;
		} else {
			return `<ul>${createList(el)}</ul>`;
		}
	}).join('');

	return newArr;
}

list.innerHTML = createList(arr);
let timerBlock = document.createElement("div");
timerBlock.style.fontSize='30px';
timerBlock.style.fontWeight='900';
timerBlock.style.padding='50px 50px';
document.body.append(timerBlock);
let timeleft = 3;
timerBlock.innerText = 'Timer: ';
let timerNumber = document.createElement("span");
timerNumber.innerText = `${timeleft}`;
timerBlock.append(timerNumber);
document.body.prepend(timerBlock);


let downloadTimer = setInterval( () =>{
     timeleft--;
     timeleft <= 0 ? clearInterval(downloadTimer) : timerNumber.innerHTML = `${timeleft}`;
}, 1000);

setTimeout(el => {
 	list.remove();
 	timerBlock.remove();
}, 3000);
